package com.sleepcore.myweatherapp.storage;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ForecastTest {

    @Test
    public void ForecastTest_MergeWithTimeSpecificForecast_ReturnsMergedForecast() {
        Forecast weeklyForecast = new Forecast();
        Forecast timeSpecificForecast = new Forecast();
        weeklyForecast.setTimeZone("America/New_York");
        weeklyForecast.setTemperatureHigh(60.0f);
        weeklyForecast.setTemperatureLow(48.2f);
        weeklyForecast.setTemperatureHighTime(1538776167);
        weeklyForecast.setTemperatureLowTime(1538777164);
        weeklyForecast.setTime(1538777295);
        weeklyForecast.setSummary("Partly cloudy");

        timeSpecificForecast.setTemperature(58.23f);
        timeSpecificForecast.setIcon("fog");

        Forecast mergedForecast = weeklyForecast.mergeWithTimeSpecificForecast(timeSpecificForecast);

        assertThat(mergedForecast.getTimeZone(), is("America/New_York"));
        assertThat(mergedForecast.getTemperatureHigh(), is(60.0f));
        assertThat(mergedForecast.getTemperatureLow(), is(48.2f));
        assertThat(mergedForecast.getTemperatureHighTime(), is(1538776167L));
        assertThat(mergedForecast.getTemperatureLowTime(), is(1538777164L));
        assertThat(mergedForecast.getTime(), is(1538777295L));
        assertThat(mergedForecast.getSummary(), is("Partly cloudy"));
        assertThat(mergedForecast.getTemperatureAtOnePm(), is(58.23f));
        assertThat(mergedForecast.getIcon(), is("fog"));

    }
}
