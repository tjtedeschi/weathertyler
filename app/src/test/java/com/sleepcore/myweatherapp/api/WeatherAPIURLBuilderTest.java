package com.sleepcore.myweatherapp.api;

import com.sleepcore.myweatherapp.api.WeatherAPIURLBuilder;

import org.junit.Test;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;

public class WeatherAPIURLBuilderTest {
    @Test
    public void weatherAPIURLBuilderTest_BuildWeeklyURL_ReturnsWeeklyURL() {
        String APIToken = "123456789abcdefghijk";
        String latitude = "12.03";
        String longitude = "-10.39";

        String builtWeeklyURL = WeatherAPIURLBuilder.buildWeeklyURL(APIToken, latitude, longitude);
        assertThat(builtWeeklyURL, is("https://api.darksky.net/forecast/123456789abcdefghijk/12.03,-10.39?exclude=hourly,minutely,alerts,flags"));
    }

    @Test
    public void weatherAPIURLBuilderTest_BuildTimeSpecificURL_ReturnsTimeSpecificURL() {
        String APIToken = "123456789abcdefghijk";
        String latitude = "12.03";
        String longitude = "-10.39";
        long timestamp = 1538774964;

        String builtTimeSpecificURL = WeatherAPIURLBuilder.buildTimeSpecificURL(APIToken, latitude, longitude, timestamp);
        assertThat(builtTimeSpecificURL, is("https://api.darksky.net/forecast/123456789abcdefghijk/12.03,-10.39,1538774964?exclude=daily,hourly,minutely,alerts,flags"));

    }
}
