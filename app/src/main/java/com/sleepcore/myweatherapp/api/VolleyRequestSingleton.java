package com.sleepcore.myweatherapp.api;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleyRequestSingleton {
    private static VolleyRequestSingleton mInstance;
    private static Context mContext;
    private RequestQueue mRequestQueue;

    private VolleyRequestSingleton(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    /**
     * Retrieves the singleton instance of this class.
     *
     * @param context the context used to get the application context
     */
    static synchronized VolleyRequestSingleton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new VolleyRequestSingleton(context);
        }
        return mInstance;
    }

    /**
     * Retrieves the current request queue, or creates one if it doesn't exist
     */
    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }

        return mRequestQueue;
    }

    /**
     * Adds a request to the current request queue
     *
     * @param request the request to be added to the queue to execute
     */
    <T> void addToRequestQueue(Request<T> request) {
        getRequestQueue().add(request);
    }
}
