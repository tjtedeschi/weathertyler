package com.sleepcore.myweatherapp.api;

public class WeatherAPIURLBuilder {
    private static final String DARK_SKY_BASE_URL = "https://api.darksky.net/forecast/";
    private static final String DAILY = "daily";
    private static final String HOURLY = "hourly";
    private static final String MINUTELY = "minutely";
    private static final String ALERTS = "alerts";
    private static final String FLAGS = "flags";
    private static final String EXCLUDE = "?exclude=";

    /**
     * Given the required components, builds a URL to request the weather API for a weekly forecast
     *
     * @param APIToken the API token used to authenticate
     * @param latitude the latitude of the location for the requested forecast
     * @param longitude the longitude of the location for the requested forecast
     */
    public static String buildWeeklyURL(String APIToken, String latitude, String longitude) {
        String URL = buildBaseLocationURL(APIToken, latitude, longitude);
        URL += EXCLUDE + HOURLY + "," + MINUTELY + "," + ALERTS + "," + FLAGS;

        return URL;
    }

    /**
     * Given the required components, builds a URL to request the weather API for a time-specific forecast
     *
     * @param APIToken the API token used to authenticate
     * @param latitude the latitude of the location for the requested forecast
     * @param longitude the longitude of the location for the requested forecast
     * @param timestamp the time of the forecast being requested
     */
    public static String buildTimeSpecificURL(String APIToken, String latitude, String longitude, long timestamp) {
        String URL = buildBaseLocationURL(APIToken, latitude, longitude);
        URL += "," + timestamp;
        URL += EXCLUDE + DAILY + "," + HOURLY + "," + MINUTELY + "," + ALERTS + "," + FLAGS;

        return URL;
    }

    /**
     * Builds the base of the URL required for any API request
     *
     * @param APIToken the API token used to authenticate
     * @param latitude the latitude of the location for the requested forecast
     * @param longitude the longitude of the location for the requested forecast
     */
    private static String buildBaseLocationURL(String APIToken, String latitude, String longitude) {
        return DARK_SKY_BASE_URL + APIToken + "/" + latitude + "," + longitude;
    }
}
