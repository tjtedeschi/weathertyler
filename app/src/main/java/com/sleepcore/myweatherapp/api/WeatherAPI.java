package com.sleepcore.myweatherapp.api;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.sleepcore.myweatherapp.R;

import org.json.JSONObject;

public class WeatherAPI {
    public static final String API_SHARED_PREFERENCES = "API_SHARED_PREFERENCES";
    public static final String API_REQUEST_COUNT_KEY = "API_REQUEST_COUNT_KEY";
    private String mLatitude, mLongitude;
    private Context mContext;

    public WeatherAPI(Context context, String latitude, String longitude) {
        mLatitude = latitude;
        mLongitude = longitude;
        mContext = context;
    }

    /**
     * Executes an API request to gather the daily forecasts for the next week
     *
     * @param successListener the listener that is triggered when the request returns a successful response
     * @param errorListener the listener that is triggered when the request returns an error response
     */
    public void getWeeklyForecasts(Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {
        String APIToken = mContext.getString(R.string.dark_sky_api_token);
        String url = WeatherAPIURLBuilder.buildWeeklyURL(APIToken, mLatitude, mLongitude);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, successListener, errorListener);

        VolleyRequestSingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
        updateAPIRequestCount();
    }

    /**
     * Executes an API request to gather a forecast at a specific time
     *
     * @param timestamp the specific time for which the forecast should be retrieved
     * @param successListener the listener that is triggered when the request returns a successful response
     * @param errorListener the listener that is triggered when the request returns an error response
     */
    public void getForecastAtTime(long timestamp, Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {
        String APIToken = mContext.getString(R.string.dark_sky_api_token);
        String url = WeatherAPIURLBuilder.buildTimeSpecificURL(APIToken, mLatitude, mLongitude, timestamp);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, successListener, errorListener);

        VolleyRequestSingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
        updateAPIRequestCount();
    }

    /**
     * Updates the value of the API request count in shared preferences by adding one to it. Called
     * after a request is sent.
     */
    private void updateAPIRequestCount() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(API_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        int APICount = sharedPreferences.getInt(API_REQUEST_COUNT_KEY, 0);
        APICount++;
        sharedPreferences.edit().putInt(API_REQUEST_COUNT_KEY, APICount).apply();

    }
}
