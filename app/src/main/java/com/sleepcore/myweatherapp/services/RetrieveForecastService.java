package com.sleepcore.myweatherapp.services;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;

import com.sleepcore.myweatherapp.R;
import com.sleepcore.myweatherapp.view_models.ForecastsViewModel;


public class RetrieveForecastService extends JobIntentService {
    private static final int FIVE_MINS_IN_MS = 300000;
    private ForecastsViewModel mForecastsViewModel;
    private Handler handler = new Handler();

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        setUpViewModel();
        handler.postDelayed(mRequestUpdate, FIVE_MINS_IN_MS);
    }

    /**
     * Sets up a Forecast View Model that houses all of the logic to make API requests and handle the
     * forecast response data
     */
    private void setUpViewModel() {
        mForecastsViewModel = new ForecastsViewModel(getApplication());
        mForecastsViewModel.setLocation(getString(R.string.paris_latitude), getString(R.string.paris_longitude));
        mForecastsViewModel.setBroadcast(true);
        mForecastsViewModel.getForecasts();
    }

    /**
     * A runnable that runs every five minutes to update the current forecast data
     */
    private Runnable mRequestUpdate = new Runnable() {
        @Override
        public void run() {
            mForecastsViewModel.getForecasts();
            handler.postDelayed(mRequestUpdate, FIVE_MINS_IN_MS);
        }
    };
}
