package com.sleepcore.myweatherapp;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sleepcore.myweatherapp.api.WeatherAPI;
import com.sleepcore.myweatherapp.services.RetrieveForecastService;
import com.sleepcore.myweatherapp.storage.Constants;
import com.sleepcore.myweatherapp.storage.Forecast;
import com.sleepcore.myweatherapp.ui.ForecastAdapter;
import com.sleepcore.myweatherapp.view_models.ForecastsViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final int FIVE_MINS_IN_MS = 300000;
    private static final int RETRIEVE_FORECAST_SERVICE_JOB_ID = 1000;
    private TextView mTvAPICounter;
    private Switch mSUseMyLocation;
    private RecyclerView mRvForecasts;
    private ForecastAdapter mForecastAdapter;
    private ForecastsViewModel mForecastsViewModel;
    private List<Forecast> mForecasts = new ArrayList<>();
    private Handler handler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUIElements();
        updateAPICounter();
        setUpViewModel();

        handler.postDelayed(mRequestUpdate, FIVE_MINS_IN_MS);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Intent intent = new Intent();
        RetrieveForecastService.enqueueWork(this, RetrieveForecastService.class, RETRIEVE_FORECAST_SERVICE_JOB_ID, intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        stopService(new Intent(this, RetrieveForecastService.class));
        registerReceiver(updatedForecastsReceiver, new IntentFilter(Constants.BROADCAST_ACTION));
        updateAPICounter();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            locationSwitched(mSUseMyLocation.isChecked());
        } else {
            mSUseMyLocation.setChecked(false);
        }
    }

    /**
     * Finds the views and sets the Activity's properties
     */
    private void setUIElements() {
        mTvAPICounter = findViewById(R.id.tvApiRequestCounter);
        mRvForecasts = findViewById(R.id.rvForecasts);
        mSUseMyLocation = findViewById(R.id.sUseMyLocation);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRvForecasts.setLayoutManager(llm);
        mForecastAdapter = new ForecastAdapter(this, mForecasts);
        mRvForecasts.setAdapter(mForecastAdapter);
        mSUseMyLocation.setChecked(false);

        mSUseMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                locationSwitched(mSUseMyLocation.isChecked());
            }
        });
    }

    /**
     * Sets up the view model with an observer for when data is changed
     */
    private void setUpViewModel() {
        mForecastsViewModel = ViewModelProviders.of(this).get(ForecastsViewModel.class);
        mForecastsViewModel.setLocation(getString(R.string.paris_latitude), getString(R.string.paris_longitude));
        mForecastsViewModel.getForecasts().observe(this, mForecastListObserver);
    }

    /**
     * Updates the API counter text view in the UI from shared preferences
     */
    private void updateAPICounter() {
        SharedPreferences sharedPreferences = getSharedPreferences(WeatherAPI.API_SHARED_PREFERENCES, MODE_PRIVATE);
        int APICount = sharedPreferences.getInt(WeatherAPI.API_REQUEST_COUNT_KEY, 0);
        mTvAPICounter.setText(String.format(getString(R.string.api_counter), APICount));
    }

    /**
     * Method called when the Use My Location Switch has changed. Will check location permissions and
     * set location to current location. Otherwise will default to Paris.
     */
    private void locationSwitched(boolean useMyLocation) {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
           ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_ID);
           mSUseMyLocation.setChecked(false);
        } else if (useMyLocation && lm != null) {
            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                setLocationToMyLocation(location);
            } else {
                setLocationToParis();
            }
        } else {
            setLocationToParis();
        }
    }

    /**
     * Sets the location of the view model to the devices current location
     */
    private void setLocationToMyLocation(Location location) {
        String myLocationLatitude = String.valueOf(location.getLatitude());
        String myLocationLongitude = String.valueOf(location.getLongitude());

        mForecastsViewModel.setLocation(myLocationLatitude, myLocationLongitude);
        mSUseMyLocation.setChecked(true);
        mForecastsViewModel.getForecasts();
    }

    /**
     * Sets the location of the view model to Paris
     */
    private void setLocationToParis() {
        String prevLatitude = mForecastsViewModel.getLatitude();
        String prevLongitude = mForecastsViewModel.getLongitude();
        if (!getString(R.string.paris_latitude).equals(prevLatitude)
                && !getString(R.string.paris_longitude).equals(prevLongitude)) {
            mForecastsViewModel.setLocation(getString(R.string.paris_latitude), getString(R.string.paris_longitude));
            mForecastsViewModel.getForecasts();
        }
        mSUseMyLocation.setChecked(false);
    }

    /**
     * Runnable that will update the forecasts every 5 minutes
     */
    private Runnable mRequestUpdate = new Runnable() {
        @Override
        public void run() {
            mForecastsViewModel.getForecasts();
            handler.postDelayed(mRequestUpdate, FIVE_MINS_IN_MS);
        }
    };

    /**
     * Observer that is triggered when the ViewModels data has changed. Updates the API counter and
     * the forecast data
     */
    private Observer<List<Forecast>> mForecastListObserver = new Observer<List<Forecast>>() {
        @Override
        public void onChanged(@Nullable List<Forecast> forecasts) {
            mForecasts.clear();
            mForecasts.addAll(forecasts);
            mForecastAdapter.notifyDataSetChanged();
            updateAPICounter();
        }
    };

    /**
     * Broadcast receiver that receives broadcasts from the background service
    * */
    private BroadcastReceiver updatedForecastsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String forecastsListJSON = intent.getStringExtra(Constants.FORECASTS_EXTRA_KEY);
            ArrayList<Forecast> forecasts = new Gson().fromJson(forecastsListJSON, new TypeToken<ArrayList<Forecast>>(){}.getType());
            if (forecasts != null) {
                mForecasts.clear();
                mForecasts.addAll(forecasts);
                mForecastAdapter.notifyDataSetChanged();
            }
        }
    };
}
