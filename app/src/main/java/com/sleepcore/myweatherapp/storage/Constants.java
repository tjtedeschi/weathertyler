package com.sleepcore.myweatherapp.storage;

public class Constants {
    public static final String BROADCAST_ACTION =
            "com.sleepscore.myweatherapp.BROADCAST";
    public static final String FORECASTS_EXTRA_KEY =
            "FORECASTS_EXTR_KEY";
    public static final int LOCATION_PERMISSION_REQUEST_ID = 1000;
}
