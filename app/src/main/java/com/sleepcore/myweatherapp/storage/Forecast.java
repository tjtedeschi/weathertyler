package com.sleepcore.myweatherapp.storage;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.format.DateFormat;

import com.sleepcore.myweatherapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Forecast {
    private float temperatureHigh;
    private float temperatureLow;
    private long temperatureHighTime;
    private long temperatureLowTime;
    private float temperatureAtOnePm;
    private float temperature;
    private long time;
    private String summary;
    private String timeZoneString;
    private String icon;

    public void setTimeZone(String timezone) { this.timeZoneString = timezone; }
    public void setIcon(String icon) { this.icon = icon; }
    public void setTime(long time) { this.time = time; }
    public void setTemperatureHigh(float temperatureHigh) { this.temperatureHigh = temperatureHigh; }
    public void setTemperatureLow(float temperatureLow) { this.temperatureLow = temperatureLow; }
    public void setTemperatureHighTime(long temperatureHighTime) { this.temperatureHighTime = temperatureHighTime; }
    public void setTemperatureLowTime(long temperatureLowTime) { this.temperatureLowTime = temperatureLowTime; }
    public void setTemperatureAtOnePm(float temperatureAtOnePm) { this.temperatureAtOnePm = temperatureAtOnePm; }
    public void setSummary(String summary) { this.summary = summary; }
    public void setTemperature(float temperature) { this.temperature = temperature; }
    public String getTimeZone() { return timeZoneString; }
    public String getIcon() { return icon; }

    public long getTime() { return time; }

    public float getTemperature() { return temperature; }

    public float getTemperatureHigh() { return temperatureHigh; }
    public float getTemperatureLow() { return temperatureLow; }
    public long getTemperatureHighTime() { return temperatureHighTime; }
    public long getTemperatureLowTime() { return temperatureLowTime; }
    public float getTemperatureAtOnePm() { return temperatureAtOnePm; }
    public String getSummary() { return summary; }

    /**
     * Formats a date for the forecast's set time
     */
    public String getDate() {
        TimeZone timeZone = TimeZone.getTimeZone(timeZoneString);
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTimeInMillis(getTime() * 1000);

        return DateFormat.format("yyyy-MM-dd", calendar).toString();
    }

    /**
     * Formats a time for the passed-in timestamp
     *
     * @param timestamp the timestamp to format
     */
    public String getFormattedTime(long timestamp) {
        TimeZone timeZone = TimeZone.getTimeZone(timeZoneString);
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTimeInMillis(timestamp * 1000);

        return DateFormat.format("hh:mm a", calendar).toString();
    }

    /**
     * Retrieves a timestamp for a string-formatted time such as: 13:00:00 on the day of the currently
     * set timestamp of the forecast.
     *
     * @param timeString the time that should translate into the timestamp for the forecast's set date
     */
    public long getTimeSpecificTimeStampOnDay(String timeString) {
        TimeZone timeZone = TimeZone.getTimeZone(timeZoneString);
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTimeInMillis(getTime() * 1000);
        String date = DateFormat.format("yyyy-MM-dd", calendar).toString();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        sdf.setTimeZone(timeZone);
        try {
            Date dayAtOnePM = sdf.parse( date + " " + timeString);
            calendar.setTime(dayAtOnePM);

            return calendar.getTimeInMillis() / 1000;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time;
    }

    /**
     * Merge a weekly forecast with a time specific forecast to gather the data necessary to report a
     * complete forecast
     *
     * @param timeSpecificForecast the time specific forecast to be merged into the weekly forecast
     */
    public Forecast mergeWithTimeSpecificForecast(Forecast timeSpecificForecast) {
        Forecast mergedForecast = new Forecast();
        mergedForecast.temperatureHigh = temperatureHigh;
        mergedForecast.temperatureLow = temperatureLow;
        mergedForecast.temperatureHighTime = temperatureHighTime;
        mergedForecast.temperatureLowTime = temperatureLowTime;
        mergedForecast.temperatureAtOnePm = timeSpecificForecast.getTemperature();
        mergedForecast.time = time;
        mergedForecast.summary = summary;
        mergedForecast.timeZoneString = timeZoneString;
        mergedForecast.icon = timeSpecificForecast.getIcon();

        return mergedForecast;
    }

    /**
     * Translates the icon string retrieved from the Weather API into an image resource
     *
     * @param context the context used to retrieve the image resource
     */
    public Drawable getIconResource(Context context) {
        if (icon != null) {
            switch(icon) {
                case ForecastIconConstants.CLEAR_DAY:
                    return context.getDrawable(R.drawable.ic_clear_day);
                case ForecastIconConstants.CLEAR_NIGHT:
                    return context.getDrawable(R.drawable.ic_clear_night);
                case ForecastIconConstants.RAIN:
                    return context.getDrawable(R.drawable.ic_rain);
                case ForecastIconConstants.SNOW:
                    return context.getDrawable(R.drawable.ic_snow);
                case ForecastIconConstants.SLEET:
                    return context.getDrawable(R.drawable.ic_sleet);
                case ForecastIconConstants.WIND:
                    return context.getDrawable(R.drawable.ic_wind);
                case ForecastIconConstants.FOG:
                    return context.getDrawable(R.drawable.ic_fog);
                case ForecastIconConstants.CLOUDY:
                    return context.getDrawable(R.drawable.ic_cloudy);
                case ForecastIconConstants.PARTLY_CLOUDY_DAY:
                    return context.getDrawable(R.drawable.ic_partly_cloudy_day);
                case ForecastIconConstants.PARTLY_CLOUDY_NIGHT:
                    return context.getDrawable(R.drawable.ic_partly_cloudy_night);
                default:
                    return context.getDrawable(R.drawable.ic_clear_day);
            }
        }

        return context.getDrawable(R.drawable.ic_clear_day);
    }
}
