package com.sleepcore.myweatherapp.view_models;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sleepcore.myweatherapp.api.WeatherAPI;
import com.sleepcore.myweatherapp.storage.Constants;
import com.sleepcore.myweatherapp.storage.Forecast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ForecastsViewModel extends AndroidViewModel {
    private static final String TAG = ForecastsViewModel.class.getSimpleName();
    private static final String ONE_PM = "13:00:00";
    private int mPendingRequestCount = 0;

    private MutableLiveData<List<Forecast>> mForecastsData;
    private List<Forecast> mForecastsList = new ArrayList<>();
    private List<Forecast> mTimeSpecificForecastsList = new ArrayList<>();
    private List<Forecast> mWeeklyForecastsList = new ArrayList<>();

    private String mLatitude, mLongitude;
    private Application mApplication;
    private boolean mSendBroadcast = false;

    public ForecastsViewModel(@NonNull Application application) {
        super(application);
        mApplication = application;
    }

    public void setLocation(String latitude, String longitude) {
        mLatitude = latitude;
        mLongitude = longitude;
    }

    public String getLatitude() { return mLatitude; }
    public String getLongitude() { return mLongitude; }

    public void setBroadcast(boolean broadcast) {
        mSendBroadcast = broadcast;
    }

    public LiveData<List<Forecast>> getForecasts() {
        if (mForecastsData == null) {
            mForecastsData = new MutableLiveData<>();
        }
        mForecastsList.clear();
        mWeeklyForecastsList.clear();
        mTimeSpecificForecastsList.clear();

        loadForecasts();

        return mForecastsData;
    }

    /**
     *  Initializes the process of loading the required forecasts by sending an API request to get the
     *  daily forecasts.
     */
    private void loadForecasts() {
        WeatherAPI weatherAPI = new WeatherAPI(mApplication.getApplicationContext(), mLatitude, mLongitude);
        weatherAPI.getWeeklyForecasts(mDailyListener, mWeeklyErrorListener);
    }

    /**
     * Submits API requests for each of the daily forecasts to get time-specific forecasts
     */
    private void loadTimeSpecificForecasts() {
        WeatherAPI weatherAPI = new WeatherAPI(mApplication.getApplicationContext(), mLatitude, mLongitude);
        for (Forecast forecast : mWeeklyForecastsList) {
            weatherAPI.getForecastAtTime(forecast.getTimeSpecificTimeStampOnDay(ONE_PM), mTimeSpecificListener, mTimeSpecificErrorListener);
        }
    }

    /**
     * Merges the daily forecasts with their respective time-specific forecasts and adds them to the
     * global list to be displayed. If the send property is set to true, this is where a broadcast
     * will be sent to any listening receivers
     */
    private void mergeTimeSpecificForecastAndInsert() {
        if (mWeeklyForecastsList.size() == mTimeSpecificForecastsList.size()) {
            for (int i = 0; i < mWeeklyForecastsList.size(); i++) {
                mForecastsList.add(mWeeklyForecastsList.get(i).mergeWithTimeSpecificForecast(mTimeSpecificForecastsList.get(i)));
            }
        }

        if (mSendBroadcast) {
            Gson gson = new GsonBuilder().create();
            String arrayListToJson = gson.toJson(mForecastsList);
            Intent localIntent = new Intent(Constants.BROADCAST_ACTION)
                    .putExtra(Constants.FORECASTS_EXTRA_KEY, arrayListToJson);
            LocalBroadcastManager.getInstance(mApplication).sendBroadcast(localIntent);
        }

        mForecastsData.setValue(mForecastsList);
    }

    /**
     * A listener that is triggered when the daily forecast requests returns a successful response.
     * Creates forecast objects and adds them to the weekly list
     */
    private Response.Listener<JSONObject> mDailyListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            try {
                String timezone = "";
                if (response.has("timezone")) {
                    timezone = response.getString("timezone");
                }
                if (response.has("daily")) {
                    JSONObject dailyJSONObject = response.getJSONObject("daily");
                    if (dailyJSONObject.has("data")) {
                        JSONArray dataJSONArray = dailyJSONObject.getJSONArray("data");
                        for (int i = 0; i < dataJSONArray.length(); i++) {
                            Forecast forecast = new Gson().fromJson(String.valueOf(dataJSONArray.getJSONObject(i)), Forecast.class);
                            forecast.setTimeZone(timezone);
                            mWeeklyForecastsList.add(forecast);
                        }
                    }
                }
                mPendingRequestCount = mWeeklyForecastsList.size();
                loadTimeSpecificForecasts();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * An error listener that is triggered when the weekly forecast request returns an error response
     * Logs the error
     */
    private Response.ErrorListener mWeeklyErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.e(TAG, error.getMessage());
        }
    };

    /**
     * A listener that is triggered when the time specific forecast request returns a successful response
     * Creates a forecast, and if all the time specific forecasts have been retrieved, merge them with
     * the weekly forecasts and insert them into the global live data list
     */
    private Response.Listener<JSONObject> mTimeSpecificListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            mPendingRequestCount--;
            try {
                String timezone = "";
                if (response.has("timezone")) {
                    timezone = response.getString("timezone");
                }
                if (response.has("currently")) {
                    JSONObject currentlyJSONObject = response.getJSONObject("currently");
                    Forecast forecast = new Gson().fromJson(String.valueOf(currentlyJSONObject), Forecast.class);
                    forecast.setTimeZone(timezone);
                    mTimeSpecificForecastsList.add(forecast);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (mPendingRequestCount == 0) {
                mergeTimeSpecificForecastAndInsert();
            }
        }
    };

    /**
     * A listener that is triggered when the time specific request returns an error response. Subtracts
     * from the pending request count and will merge the existing forecasts if there are no more requests
     * to process. (This will get a bit buggy the way I have the merge function...) ideally I would have
     * some sort of id or way to check equality.
     */
    private Response.ErrorListener mTimeSpecificErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            mPendingRequestCount--;
            if (mPendingRequestCount == 0) {
                mergeTimeSpecificForecastAndInsert();
            }
            Log.e(TAG, error.toString());
        }
    };
}
