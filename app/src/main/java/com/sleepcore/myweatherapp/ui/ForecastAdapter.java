package com.sleepcore.myweatherapp.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.sleepcore.myweatherapp.R;
import com.sleepcore.myweatherapp.storage.Forecast;

import java.util.List;

public class ForecastAdapter extends RecyclerView.Adapter {
    private List<Forecast> mForecastsList;
    private LayoutInflater mInflater;
    private Context mContext;

    private static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTvHighTemperature;
        TextView mTvLowTemperature;
        TextView mTvMidDayTemperature;
        TextView mTvSummary;
        TextView mTvHighTemperatureTime;
        TextView mTvLowTemperatureTime;
        TextView mTvCurrentDate;
        FrameLayout mFlWeatherIcon;

        ViewHolder(View v) {
            super(v);
            mTvHighTemperature = v.findViewById(R.id.tvHighTemperature);
            mTvHighTemperatureTime = v.findViewById(R.id.tvHighTemperatureTime);
            mTvLowTemperature = v.findViewById(R.id.tvLowTemperature);
            mTvLowTemperatureTime = v.findViewById(R.id.tvLowTemperatureTime);
            mTvMidDayTemperature = v.findViewById(R.id.tvMidDayTemperature);
            mTvSummary = v.findViewById(R.id.tvSummary);
            mTvCurrentDate = v.findViewById(R.id.tvCurrentDate);
            mFlWeatherIcon = v.findViewById(R.id.flWeatherIcon);
        }
    }

    public ForecastAdapter(Context context, List<Forecast> forecasts) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mForecastsList = forecasts;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.card_forecast, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            TextView tvHighTemperature = ((ViewHolder) holder).mTvHighTemperature;
            TextView tvHighTemperatureTime = ((ViewHolder) holder).mTvHighTemperatureTime;
            TextView tvLowTemperature = ((ViewHolder) holder).mTvLowTemperature;
            TextView tvLowTemperatureDate = ((ViewHolder) holder).mTvLowTemperatureTime;
            TextView tvMidDayTemperature = ((ViewHolder) holder).mTvMidDayTemperature;
            TextView tvSummary = ((ViewHolder) holder).mTvSummary;
            TextView tvCurrentDate = ((ViewHolder) holder).mTvCurrentDate;
            FrameLayout flWeatherIcon = ((ViewHolder) holder).mFlWeatherIcon;

            final Forecast forecast = mForecastsList.get(position);

            tvHighTemperature.setText(
                    String.format(mContext.getString(R.string.high_temperature), (int) forecast.getTemperatureHigh())
            );
            tvHighTemperatureTime.setText(
                    String.format(mContext.getString(R.string.at), forecast.getFormattedTime(forecast.getTemperatureHighTime()))
            );
            tvLowTemperature.setText(
                    String.format(mContext.getString(R.string.low_temperature), (int) forecast.getTemperatureLow())
            );
            tvLowTemperatureDate.setText(
                    String.format(mContext.getString(R.string.at), forecast.getFormattedTime(forecast.getTemperatureLowTime()))
            );
            tvMidDayTemperature.setText(
                    String.format(mContext.getString(R.string.onepm_temperature), (int) forecast.getTemperatureAtOnePm())
            );
            tvSummary.setText(forecast.getSummary());
            tvCurrentDate.setText(forecast.getDate());
            setWeatherIcon(forecast, flWeatherIcon);
        }
    }

    @Override
    public int getItemCount() {
        return mForecastsList.size();
    }

    /**
     * Creates an image view to insert into the frame layout for the current weather icon
     *
     * @param forecast the forecast from which to retrieve the icon string
     * @param flWeatherIcon the frame layout in which to insert the image view
     */
    private void setWeatherIcon(Forecast forecast, FrameLayout flWeatherIcon) {
        flWeatherIcon.removeAllViews();
        ImageView ivWeatherIcon = new ImageView(mContext);
        ivWeatherIcon.setImageDrawable(forecast.getIconResource(mContext));
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT
        );
        flWeatherIcon.addView(ivWeatherIcon, layoutParams);
    }
}
